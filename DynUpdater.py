from playwright.sync_api import sync_playwright
import os
import os.path
from datetime import datetime
import requests

C_IP_ADDR = "192.168.0.1" # Your router ip
C_CLIENT_KEY = "" # Dyn.com client key
C_ROUTER_PASSWORD = "" # Router password
C_LOG_FILE = os.getcwd() + "/" + "dyn.log"
C_IP_FILE = os.getcwd() + "/" + "ip.txt"
C_HOSTNAME = "xxx.homelinux.com" # dyn.com dns name
C_DYN_USERNAME = "dynuser" # dyn.com username


def getIP(p_ip,p_router_pwd) : 
    playwright = sync_playwright().start()
    browser = playwright.firefox.launch()
    page = browser.new_page()
    page.goto("http://192.168.0.1")
    page.fill('input#pc-login-password',p_router_pwd)
    page.click('button[type=button]')
    page.is_visible('div.verticalFixed')
    loc = page.input_value('input#IPV4')
    text = loc
    page.close()
    browser.close()
    return text


def log(p_log_file,p_str) :
    with open(p_log_file, 'a') as file:
        file.write(get_now() + " -- " + p_str + "\n")

def get_ip_from_file(p_file) :
    if(os.path.isfile(p_file)) :
        v_ip_file_old = open(C_IP_FILE,'r')
        v_ip_old = v_ip_file_old.read()
        v_ip_file_old.close()
        return v_ip_old
    else :
        return "0.0.0.0"

def set_ip_in_file(p_ip_file,p_ip) :
    v_ip_file = open(p_ip_file, 'w')
    v_ip_file.write(p_ip)
    v_ip_file.close()

def process() :
    v_ip_address = getIP(C_IP_ADDR,C_ROUTER_PASSWORD)
    v_ip_address_old = get_ip_from_file(C_IP_FILE)    
    if(v_ip_address != v_ip_address_old) :
        set_ip_in_file(C_IP_FILE,v_ip_address)
        log(C_LOG_FILE,"**** IP address different old is " + v_ip_address_old + " new is " + v_ip_address )
        set_ip_in_dyn(C_CLIENT_KEY,C_HOSTNAME,v_ip_address,C_DYN_USERNAME)
    else :
        log(C_LOG_FILE,"IP address same")
        print("Nothing to update")
        
    print("Finished")
    

        

# datetime object containing current date and time

def get_now() :
    return (datetime.now().strftime("%d/%m/%Y %H:%M:%S"))

def set_ip_in_dyn(p_client_key,p_host_name,p_ip_address,p_username) :
    v_url = "https://" + p_username + ":" + p_client_key + "@members.dyndns.org/nic/update?hostname=" + p_host_name + "&myip=" + p_ip_address
    resp = requests.get(url = v_url)
    log(C_LOG_FILE,"Response from dyn.com")
    log(C_LOG_FILE,resp.text) 
    print("Updating DNS")
    
def main():
    process()

if __name__ == "__main__":
    main()    

#await process()
